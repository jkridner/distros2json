"""
Converts W3C Microdata HTML documents to bb-imager schema JSON documents

What is Microdata? https://www.w3.org/TR/2021/NOTE-microdata-20210128/
"""
import json
import extruct

def extract(infile, outfile, midfile=None):
  """
  Extract microdata from HTML infile, write to JSON outfile and optionally save intermediate
  JSON file with directly extracted microdata to midfile
  """
  with open(infile, 'r', encoding='utf-8') as f:
    distros = f.read()
    mde = extruct.w3cmicrodata.MicrodataExtractor()
    data = mde.extract(distros)
  if midfile:
    with open(midfile, 'w', encoding='utf-8') as f:
      print(json.dumps(data, indent=2), file=f)
  data = convert(data)
  with open(outfile, 'w', encoding='utf-8') as f:
    print(json.dumps(data, indent=2), file=f)

def convert(data):
  """
  Convert dict data from the microdata schema to the bb-imager schema and return it
  """
  c = {}
  imager = {
    "latest_version": "2.0.0",
    "url": "https://www.beagleboard.org/distros.json",
    "devices": [
      {
        "name": "No filtering",
        "tags": [],
        "default": False,
        "description": "Show every possible image",
        "matching_type": "inclusive"
      },
      {
        "name": "BeagleY-AI",
        "tags": [
          "beagle-am67"
        ],
        "default": False,
        "icon":
          "https://www.beagleboard.org/app/uploads/2024/03/BeagleY-AI-angled-front-1-400x267.webp",
        "description": "BeagleY-AI based on TI AM67A",
        "matching_type": "inclusive"
      },
      {
        "name": "BeaglePlay",
        "tags": [
          "beagle-am62"
        ],
          "icon": "https://www.beagleboard.org/app/uploads/2023/03/45front-400x297.webp",
          "description": "BeaglePlay based on TI AM62",
          "matching_type": "exclusive"
      },
      {
        "name": "BeagleBone AI-64",
        "tags": [
          "beagle-tda4vm"
        ],
        "default": False,
        "icon": "https://www.beagleboard.org/app/uploads/2022/04/BeagleBone_AI_64-400x333.webp",
        "description": "BeagleBone AI-64 based on TI TDA4VM",
        "matching_type": "inclusive"
      },
      {
        "name": "BeagleBone Black",
        "tags": [
          "beagle-am335"
        ],
        "default": False,
        "icon": "https://www.beagleboard.org/app/uploads/2023/07/DSC00505-400x345.webp",
        "description": "BeagleBone Black, PocketBeagle and other boards based on TI AM335",
        "matching_type": "inclusive"
      }
    ]
  }
  c["imager"] = imager
  os_list = []
  for j in data:
    if j["type"] == "https://schema.org/SoftwareApplication":
      i = j["properties"]
      d = {}
      d["name"] = o(i,"name")
      d["description"] = o(i,"description") + \
        "\nVersion: " + o(i,"version") + "\nFeatures: " + str(o(i,"featureList")) + \
        "\nStorage memory requirements: " + o(i,"memoryRequirements") + \
        "\nProject URL: " + o(i,"url")
      d["icon"] = o(i,"thumbnailUrl")
      d["url"] = o(i,"downloadUrl")
      d["extract_size"] = o(i,"extractedSize")
      d["extract_sha256"] = o(i,"extractedSha256sum")
      d["image_download_size"] = o(i,"fileSize")
      d["image_download_sha256"] = o(i,"fileSha256sum")
      d["release_date"] = o(i,"datePublished")
      d["devices"] = o(i,"availableOnDevice")
      os_list.append(d)
  c["os_list"] = os_list
  return c

def o(i, j):
  """
  Extract node j out of dict i and return it or return an empty string if it doesn't exist
  """
  try:
    k = i[j]
    if k:
      return k
  except KeyError:
    return ""
  return ""

extract('latest-images.html', 'latest-images.json', 'latest-images.schema.json')
extract('latest-images-fixed.html', 'latest-images-fixed.json', 'latest-images-fixed.schema.json')
extract('distros.html', 'distros.json', 'distros.schema.json')
extract('distros-fixed.html', 'distros-fixed.json', 'distros-fixed.schema.json')
